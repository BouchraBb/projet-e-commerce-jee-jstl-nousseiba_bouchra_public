package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dto.AchatDto;
import dto.ClientDto;
import dto.PanierDto;
import dto.ProduitDto;
import model.Achat;
import model.Client;
import model.Panier;
import model.Produit;
import service.AchatService;
import service.PanierService;
import service.ProduitService;

/**
 * Servlet implementation class PanierServlet
 */
public class PanierServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	PanierService panierService = new PanierService();
	ProduitService produitService = new ProduitService();
	AchatService achatService = new AchatService();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PanierServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Integer idProduit = null ; 
		Integer idAchat = null ; 
		HttpSession session = request.getSession();
		ClientDto clientSesion = (ClientDto) session.getAttribute("clientSession");
		
	    PanierDto panierSession = (PanierDto)session.getAttribute("panierSession");

		List<ProduitDto>produits = (List<ProduitDto>) session.getAttribute("produits");
		produits = produitService.findAll();
		
		if (clientSesion!= null ) {
			
			String operation = request.getParameter("operation");
			String idProd = request.getParameter("id");
			if(idProd!=null) { idProduit =Integer.parseInt(idProd );}
			
			String idAch = request.getParameter("idAchat");
			if(idAch!=null) { idAchat =Integer.parseInt(idAch );}
			
			
			if(operation!=null && operation.equalsIgnoreCase("addProduct")&& idProduit!=null) {
				ProduitDto produit = produitService.findById(idProduit);
				produit.setQuantiteDispo(produit.getQuantiteDispo()-1);
                produitService.update(produit);
                AchatDto achat = null;
                achat = achatService.findAchatByProduit(idProduit, ((PanierDto) session.getAttribute("panierSession")).getIdPanier());
                if(achat==null){
                	achat = new AchatDto( produit, 1, (PanierDto)session.getAttribute("panierSession"));
                	achatService.save(achat);
                	((PanierDto) session.getAttribute("panierSession")).addachat(achat);
                }

	
                else{
                achat.setQuantite(achat.getQuantite()+1);
                achatService.update(achat);
                }
                PanierDto panier = panierService.findById(((PanierDto) session.getAttribute("panierSession")).getIdPanier());
				panierService.update(panier);
				produits = produitService.findAll();
				
	         
	          // session.setAttribute("panierSession", panierSession);
	           session.setAttribute("produits", produits);
	           double totale1 = panierService.calculTotale( panierSession);
               double totale = (int)(Math.round(totale1 * 100))/100.0;
               produits = produitService.findAll();
               session.setAttribute("totale", totale);
	            this.getServletContext().getRequestDispatcher("/WEB-INF/select_produit.jsp").forward(request, response);
	            

			} else if(operation!=null && operation.equalsIgnoreCase("afficher")) {
				double totale1 = panierService.calculTotale( panierSession);
                double totale = (int)(Math.round(totale1 * 100))/100.0;
                produits = produitService.findAll();
                session.setAttribute("totale", totale);
				 this.getServletContext().getRequestDispatcher("/WEB-INF/afficher_panier.jsp").forward(request, response);
				
			}
			
			else if(operation!=null && operation.equalsIgnoreCase("addQuantity")&& idAchat!= null ) {
				System.out.println((Panier) session.getAttribute("panierSession"));
				
				
				PanierDto panier = panierService.findById(((PanierDto) session.getAttribute("panierSession")).getIdPanier());
				panierSession= panier; 
				
				AchatDto achat =achatService.findById(idAchat);
				achat.setQuantite(achat.getQuantite()+1);
				achatService.update(achat);
				//achat =achatService.findById(idAchat);
	            panierService.update(panier);
	            panier = panierService.findById(((Panier) session.getAttribute("panierSession")).getIdPanier());
	            panierSession = panier; 
	            System.out.println(panierSession);
	  
				ProduitDto produit = produitService.findById(achat.getProduit().getIdProduit());
				produit.setQuantiteDispo(produit.getQuantiteDispo()-1);
                produitService.update(produit);
                
                produits = produitService.findAll();
                
                panierSession = panierService.findById(((Panier) session.getAttribute("panierSession")).getIdPanier());
                System.out.println(panierSession);
				 session.setAttribute("panierSession", panierSession);
		           session.setAttribute("produits", produits);
		           double totale1 = panierService.calculTotale( panierSession);
	                double totale = (int)(Math.round(totale1 * 100))/100.0;
	                produits = produitService.findAll();
	                session.setAttribute("totale", totale);
		           System.out.println((Panier) session.getAttribute("panierSession"));
				 this.getServletContext().getRequestDispatcher("/WEB-INF/afficher_panier.jsp").forward(request, response);
				
			}else if(operation!=null && operation.equalsIgnoreCase("reduceQuantity")&& idAchat!= null ) {
				 panierSession = (PanierDto) session.getAttribute("panierSession");
				AchatDto achat =achatService.findById(idAchat);
				achat.setQuantite(achat.getQuantite()-1);
				achatService.update(achat);
				achat =achatService.findById(idAchat);
				
				
				panierService.update(panierSession);
				panierSession = panierService.findById(panierSession.getIdPanier());
				 System.out.println(panierSession);
				 

					ProduitDto produit = produitService.findById(achat.getProduit().getIdProduit());
					produit.setQuantiteDispo(produit.getQuantiteDispo()+1);
	                produitService.update(produit);
	                double totale1 = panierService.calculTotale( panierSession);
	                double totale = (int)(Math.round(totale1 * 100))/100.0;
	                produits = produitService.findAll();
	                session.setAttribute("totale", totale);
	                session.setAttribute("produits", produits);
				 session.setAttribute("panierSession", panierSession);
				 this.getServletContext().getRequestDispatcher("/WEB-INF/afficher_panier.jsp").forward(request, response);
			}
			
			}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
