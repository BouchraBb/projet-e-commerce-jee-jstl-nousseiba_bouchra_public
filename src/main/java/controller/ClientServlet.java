package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dto.ClientDto;
import dto.PanierDto;
import model.Client;
import model.Panier;
import service.ClientService;
import service.PanierService;

/**
 * Servlet implementation class ClientServlet
 */
public class ClientServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	ClientService clientService = new ClientService();
	PanierService panierService = new PanierService();

	/**
	 * Default constructor.
	 */
	public ClientServlet() {
		// TODO Auto-generated constructor stub
	}

	ClientDto clientSession = null;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		String operation = request.getParameter("operation");

		if (operation != null && operation.equalsIgnoreCase("disconnect")) {
			resitSession(session);
			this.getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
		}

		else if (operation != null && operation.equalsIgnoreCase("afficher")) {
			session.setAttribute("clients", clientService.findAll());
			this.getServletContext().getRequestDispatcher("/WEB-INF/display_clients.jsp").forward(request, response);

		} else if (operation != null && operation.equalsIgnoreCase("unsubscribe")) {
			clientService.unsubscribe(clientSession);
			resitSession(session);
			String message = "Vous êtes désinscit de notre site ";
			request.setAttribute("message", message);
			this.getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
		}

	}

	private void resitSession(HttpSession session) {
		session.setAttribute("clientSession", null);
		session.setAttribute("panierSession", null);
		session.setAttribute("totale", null);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String adresse = request.getParameter("adresse");
		String tel = request.getParameter("tel");
		String mail = request.getParameter("email");
		String operation = request.getParameter("operation");
		String motDePass = request.getParameter("motDePasse");

		if(operation != null && operation.equalsIgnoreCase("create")) {
			clientService.createClient(nom, prenom, adresse, mail, tel, motDePass);
				clientSession = clientService.findByMail(mail, motDePass);
				session.setAttribute("clientSession", clientSession);
				session.setAttribute("panierSession", panierService.getPanierSession(clientSession));
				this.getServletContext().getRequestDispatcher("/WEB-INF/client.jsp").forward(request, response);

		} else if (operation != null && operation.equalsIgnoreCase("connect")) {

			if (mail != null && motDePass != null) {
				ClientDto clientConnect = clientService.findByMail(mail, motDePass);
				if (clientConnect != null) {
					clientSession = clientConnect;

					session.setAttribute("clientSession", clientSession);
					session.setAttribute("panierSession", panierService.getPanierSession(clientConnect));
					this.getServletContext().getRequestDispatcher("/WEB-INF/client.jsp").forward(request, response);
				} else {
					String message = "Votre identifiant ou votre mot de passe n'est pas correct !  ";
					request.setAttribute("message", message);
					this.getServletContext().getRequestDispatcher("/formulaire_connexion.jsp").forward(request,
							response);
				}
			}

		}

	}

}
