package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JCheckBox;


import dto.ProduitDto;
import model.Produit;
import service.ProduitService;
import util.Constante;

/**
 * Servlet implementation class ProduitServlet
 */
public class ProduitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	ProduitService produitService = new ProduitService();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ProduitServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		String op2 = request.getParameter("op2");
		String operation = request.getParameter("operation");
		String idS = request.getParameter("id");
		Integer id=null;
		if (idS != null) {
			id = Integer.parseInt(idS);
		}

		if (operation != null && operation.equalsIgnoreCase("afficher")&&op2==null) {
			
			session.setAttribute("produits", produitService.findProducts());
			
			this.getServletContext().getRequestDispatcher("/display_products.jsp").forward(request, response);

		}
		else if (op2!=null && op2.equalsIgnoreCase("gerer")) {
			
			session.setAttribute("produits", produitService.findAll());
			
			this.getServletContext().getRequestDispatcher("/WEB-INF/gerer_produit.jsp").forward(request, response);

		}else if (op2!=null && op2.equalsIgnoreCase("creer")) {
			this.getServletContext().getRequestDispatcher("/WEB-INF/ajout_produit.jsp").forward(request, response);

		}
		else if (operation != null && operation.equals("delete") && id != null) {
			ProduitDto produit = produitService.findById(id);
			produit.setDeleted(true);
			produitService.update(produit);
			session.setAttribute("produits", produitService.findAll());
			request.setAttribute("message", Constante.MESSAGE_CONFIRM);
			this.getServletContext().getRequestDispatcher("/WEB-INF/gerer_produit.jsp").forward(request, response);

		} else if (operation != null && operation.equals("update") && id != null) {
			ProduitDto produit = produitService.findById(id);
			request.setAttribute("produitToUpdate", produit);
			this.getServletContext().getRequestDispatcher("/WEB-INF/update_produit.jsp").forward(request, response);

		}
		else if (operation != null && operation.equalsIgnoreCase("afficher")&&op2!=null && op2.equalsIgnoreCase("select")) {
			
			session.setAttribute("produits", produitService.findProducts());
			
			this.getServletContext().getRequestDispatcher("/WEB-INF/select_produit.jsp").forward(request, response);

		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		String nom = request.getParameter("nom");
		String prix = request.getParameter("prix");
		String quantite = request.getParameter("quantite");
		JCheckBox checkbox = new JCheckBox();
		String operation = request.getParameter("operation");
		String idS = request.getParameter("id");
		Integer id= null ; 
		if(idS!=null) {
			id= Integer.parseInt(idS);
		}
		
		if (operation != null && operation.equalsIgnoreCase("create")) {

		ProduitDto produit = new ProduitDto(nom, Double.parseDouble(prix), Integer.parseInt(quantite));
		produitService.save(produit);
		request.setAttribute("produit", produit);

		request.setAttribute("message", Constante.MESSAGE_CONFIRM);
		this.getServletContext().getRequestDispatcher("/WEB-INF/admin.jsp").forward(request, response);
		
		} else if (operation!= null && operation.equalsIgnoreCase("update")& id!=null){
			ProduitDto produit = produitService.findById(id);
			System.out.println(produit );
			produit.setNom(nom);
			produit.setPrix( Double.parseDouble(prix));
			produit.setQuantiteDispo(Integer.parseInt(quantite));
			if(!checkbox.isSelected()) {
				produit.setDeleted(false);
			}
			produitService.update(produit);
			System.out.println(produit.getDeleted());
			List<ProduitDto>produits = produitService.findAll();
			session.setAttribute("produits", produits);
			request.setAttribute("message", Constante.MESSAGE_CONFIRM);
			this.getServletContext().getRequestDispatcher("/WEB-INF/gerer_produit.jsp").forward(request, response);
		}

	}

}
