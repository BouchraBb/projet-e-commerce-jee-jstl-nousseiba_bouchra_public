package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dto.ClientDto;
import dto.CommandeDto;
import dto.PanierDto;
import model.Client;
import model.Commande;
import model.Panier;
import service.ClientService;
import service.CommandeService;
import service.PanierService;

/**
 * Servlet implementation class CommandeServlet
 */
public class CommandeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	ClientService clientService = new ClientService();
	CommandeService commandeService = new CommandeService();
	PanierService panierService = new PanierService();


	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CommandeServlet() {
		
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		String operation = request.getParameter("operation");
		ClientDto clientSession = (ClientDto) session.getAttribute("clientSession");
		PanierDto panierSession = (PanierDto) session.getAttribute("panierSession");
		
		if(session.getAttribute("totale")!=null) {
			Double totale=   (Double) session.getAttribute("totale");
		
		if (operation != null && operation.equalsIgnoreCase("commander")) {
			System.out.println(panierSession);
			PanierDto panierValide= panierSession;
			CommandeDto commande = commandeService.validerPanier(panierSession, totale, clientSession);
			panierSession = panierService.getPanierSession(clientSession);
			System.out.println(panierSession);

			session.setAttribute("panierSession", panierSession);
			session.setAttribute("commande", commande);
			session.setAttribute("totale", totale);
			String cb = request.getParameter("cb").trim();
			session.setAttribute("cb",  "**** **** ****" +cb.substring(13, 16));
			request.setAttribute("panierValide", panierValide);
			this.getServletContext().getRequestDispatcher("/WEB-INF/commande.jsp").forward(request, response);
		
		}
		else if (operation != null && operation.equalsIgnoreCase("payer")) {
		
				this.getServletContext().getRequestDispatcher("/WEB-INF/paiement.jsp").forward(request, response);
		}
		}
		
		 if (operation != null && operation.equalsIgnoreCase("afficher")) {
			session.setAttribute("commandes", commandeService.findAll());

			this.getServletContext().getRequestDispatcher("/WEB-INF/display_commandes.jsp").forward(request, response);
		}
			else if (operation != null && operation.equalsIgnoreCase("afficherForClient")) {
				session.setAttribute("commandes", commandeService.findAllCommandesByClient(clientSession.getMail()));

				this.getServletContext().getRequestDispatcher("/WEB-INF/display_commandes_client.jsp").forward(request, response);
			}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
