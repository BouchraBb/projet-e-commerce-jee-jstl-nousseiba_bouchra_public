package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import model.Admin;
import service.AdminService;

/**
 * Servlet implementation class AdminServlet
 */
public class AdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	AdminService adminService = new AdminService();
	Admin adminSession = null;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdminServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String operation = request.getParameter("operation");
		System.out.println(operation);
		if (operation != null && operation.equalsIgnoreCase("disconnect")) {
			HttpSession session = request.getSession();
			session.setAttribute("adminSession", null);
			this.getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String mail = request.getParameter("mail");
		String motDePass = request.getParameter("motDePasse");
		String operation = request.getParameter("operation");

		if (operation != null && operation.equalsIgnoreCase("create")) {
			Admin admin = new Admin(mail, motDePass);
			adminService.save(admin);
			Admin adminConnect1 = adminService.findByMail(mail, motDePass);
			if (adminConnect1 != null) {
				adminSession = adminConnect1;
				HttpSession session = request.getSession();
				session.setAttribute("adminSession", adminSession);
				String message = "votre compte a bien été creé ";
				request.setAttribute("message", message);
				this.getServletContext().getRequestDispatcher("/WEB-INF/admin.jsp").forward(request, response);
			}
			
		}

		else if (operation != null && operation.equalsIgnoreCase("connect")) {
			mail = request.getParameter("mail");
			motDePass = request.getParameter("motDePasse");
			if (mail != null && motDePass != null) {

				Admin adminConnect = adminService.findByMail(mail, motDePass);
				if (adminConnect != null) {
					adminSession = adminConnect;
					HttpSession session = request.getSession();
					session.setAttribute("adminSession", adminSession);
					this.getServletContext().getRequestDispatcher("/WEB-INF/admin.jsp").forward(request, response);

				}

			}
		
	}
	}
}
