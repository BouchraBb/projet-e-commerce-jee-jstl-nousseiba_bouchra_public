package dto;

import lombok.*;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Setter
@Getter
public class PanierDto {

    private List<AchatDto> listeAchats= new ArrayList<>();
    ClientDto client;
    private boolean valide ;
    private Integer idPanier;

    public PanierDto(ClientDto client) {
        this.client = client;
        this.valide=false;
    }

    public void addachat(AchatDto achat){
        listeAchats.add(achat);
    }

    public void removeachat(AchatDto achat){
        listeAchats.remove(achat);
    }

}
