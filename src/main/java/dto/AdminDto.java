package dto;

import lombok.*;

import javax.persistence.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Setter
@Getter
public class AdminDto {
	private Integer idAdmin;
		private String mail;
	    private String motDePasse ;

		public AdminDto(String mail, String motDePasse) {
			super();
			this.mail = mail;
			this.motDePasse = motDePasse;
		}

}
