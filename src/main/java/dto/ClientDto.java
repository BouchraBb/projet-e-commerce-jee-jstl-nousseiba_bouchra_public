package dto;
import lombok.*;

import javax.persistence.Entity;
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Setter
@Getter
public class ClientDto {
    private Integer idClient;
    private String nom;
    private String prenom;
    private String adresse;
    private String mail;
    private String tel;
    private String motDePasse;
    public ClientDto (String nom, String prenom, String adresse, String mail, String tel, String motDePasse){
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.mail = mail;
        this.motDePasse= motDePasse;
        this.tel= tel ;
    }





}
