package dto;

import lombok.*;
import model.Client;
import model.Panier;

import javax.persistence.*;
import java.sql.Date;


@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Setter
@Getter
public class CommandeDto {
    private Integer idCommande;
    private Double total;
    private Date date;
    private PanierDto panier;

    private ClientDto client;


    public CommandeDto(Double total, Date date, PanierDto panier, ClientDto client) {
        this.total = total;
        this.date = date;
        this.panier = panier;
        this.client=client;
    }

    @Override
    public String toString() {
        return "CommandeDto{" +
                "idCommande=" + idCommande +
                ", total=" + total +
                ", date=" + date +
                ", panierDto=" + panier +
                ", client=" + client +
                '}';
    }
}
