package dto;

import lombok.*;
import model.Achat;
import model.Panier;
import model.Produit;

import javax.persistence.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Setter
@Getter
public class AchatDto {
    private Integer idAchat;
    private ProduitDto produit;
    private PanierDto panier;
    private Integer quantite;



    public AchatDto(ProduitDto produit, int quantite, PanierDto panier) {
        this.produit = produit;
        this.quantite = quantite;
        this.panier = panier;
    }

    @Override
    public String toString() {
        return "AchatDto{" +
                "idAchat=" + idAchat +
                ", produit=" + produit +
                ", quantite=" + quantite +
                '}';
    }
}
