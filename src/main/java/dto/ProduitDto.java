package dto;

import lombok.*;
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Setter
@Getter
public class ProduitDto {
    private String nom;
    private Double prix;
    private Integer quantiteDispo;
    private Boolean deleted;
    private Integer idProduit;

    public ProduitDto(String nom, Double prix, Integer quantiteDispo) {
        this.nom = nom;
        this.prix = prix;
        this.quantiteDispo = quantiteDispo;
        this.deleted=false;
      
    }
}
