package dao;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;

import util.HibernateUtil;

public class GenericDAO<T> {
	protected static Session session = HibernateUtil.getSessionFactory().openSession();
	private Class<T> entity;

	public GenericDAO(Class<T> entity) {
		this.entity = entity;
	}

	public void save(T obj) {
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.saveOrUpdate(obj);
			transaction.commit();

		} catch (Exception e) {
			if (transaction != null) {
				System.out.println("error create");
				transaction.rollback();
			}
		}
	}

	public void update(T obj) {
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.saveOrUpdate(obj);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				System.out.println("error update");
				tx.rollback();
			}

		}
	}

	public void delete(T obj) {
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.delete(obj);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				System.out.println("error delete");
				tx.rollback();
			}

		}
	}

	public T findById(int id) {

//        session.getTransaction().begin();       OR session.beginTransaction();
//       ...   to do ....
//        session.getTransaction().commit();

		return (T) session.get(entity, id);
	}

	public List<T> findAll() {
		return (List<T>) session.createQuery("from " + entity.getName()).list();
	}

}
