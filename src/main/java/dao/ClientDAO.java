package dao;

import model.Client;
import model.Panier;
import org.hibernate.Query;
import org.hibernate.Transaction;

import java.util.Arrays;

public class ClientDAO extends GenericDAO<Client>{
    public ClientDAO() {
        super(Client.class);
    }

    public Client findByMail(String mail, String mdp) {
        Transaction transaction = session.beginTransaction();
        Query query = session.getNamedQuery("findClientByMail");
        query.setParameter("mail", mail);
        query.setParameter("mdp", mdp);
        Client client  = (Client) query.uniqueResult();
        transaction.commit();

        return client;

    }


    public Object findByMailOnly(String mail) {
        Transaction transaction = session.beginTransaction();
        Query query = session.getNamedQuery("findByMailOnly");
        query.setParameter("mail", mail);
        Client client  = (Client) query.uniqueResult();
        transaction.commit();

        return client;
    }
}
