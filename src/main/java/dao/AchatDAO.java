package dao;

import org.hibernate.Query;
import org.hibernate.Transaction;

import model.Achat;
import model.Client;

public class AchatDAO extends GenericDAO<Achat>{
    public AchatDAO() {
        super(Achat.class);
    }
    public Achat findAchatByProduit(int idProduit, int idPanier) {
        Transaction transaction = session.beginTransaction();
        Query query = session.getNamedQuery("findAchatByProduit");
        query.setParameter("idProduit", idProduit);
        query.setParameter("idPanier", idPanier);
        Achat achat  = (Achat) query.uniqueResult();
        transaction.commit();
        return achat;
      
    }

    
}
