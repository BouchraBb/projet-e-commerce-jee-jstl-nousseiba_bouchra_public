package dao;

import dto.PanierDto;
import model.Panier;
import org.hibernate.Query;
import org.hibernate.Transaction;

public class PanierDAO extends GenericDAO<Panier>{

    public PanierDAO() {
        super(Panier.class);
    }


    public Panier findByMailClient(String mail) {
        Transaction transaction = session.beginTransaction();
        Query query = session.getNamedQuery("findByMailClient");
        query.setParameter("mail", mail);
        Panier panier  = (Panier) query.uniqueResult();
        transaction.commit();
        return panier;
    }
}
