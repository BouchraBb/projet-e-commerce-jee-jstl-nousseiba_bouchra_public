package dao;

import org.hibernate.Query;
import org.hibernate.Transaction;

import model.Admin;


public class AdminDAO extends GenericDAO<Admin> {

	public AdminDAO() {
		super(Admin.class);
		
	}

	
	  public Admin findByMail(String mail, String mdp) {
	        Transaction transaction = session.beginTransaction();
	        Query query = session.getNamedQuery("findAdminByMail");
	        query.setParameter("mail", mail);
	        query.setParameter("mdp", mdp);
	        Admin admin  = (Admin) query.uniqueResult();
	        transaction.commit();

	        return admin;

	    }

}
