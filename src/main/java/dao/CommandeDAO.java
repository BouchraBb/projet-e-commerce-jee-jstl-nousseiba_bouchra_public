package dao;

import model.Commande;
import org.hibernate.Query;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class CommandeDAO extends  GenericDAO<Commande> {
    public CommandeDAO() {
        super(Commande.class);
    }


    public List<Commande> findAllCommandesByClient(String mail){
        Transaction transaction=super.session.beginTransaction();
        Query query=session.getNamedQuery("findAllCommandesByClient");
        query.setParameter("mail",mail);
        List<Commande> commandes=(ArrayList<Commande>) query.list();
        transaction.commit();
        return commandes;
    }
}
