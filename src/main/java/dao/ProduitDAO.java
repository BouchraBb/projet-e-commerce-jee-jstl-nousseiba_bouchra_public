package dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Transaction;

import model.Panier;
import model.Produit;

public class ProduitDAO extends GenericDAO<Produit> {
    public ProduitDAO( ) {
        super(Produit.class);
    }
    
    
    public List<Produit> findProducts() {
        Transaction transaction = session.beginTransaction();
        Query query = session.getNamedQuery("findProducts");
        List<Produit> produits  = (List<Produit>) query.list();
        transaction.commit();
        return produits;
    }


    public Produit findProductByCode(int code) {
        Transaction transaction = session.beginTransaction();
        Query query = session.getNamedQuery("findProductByCode");
        query.setParameter("code", code);
        Produit produit  = (Produit) query.uniqueResult();
        transaction.commit();
        return produit;
    }
}
