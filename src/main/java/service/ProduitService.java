package service;

import dao.ProduitDAO;
import dto.ProduitDto;
import model.Produit;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.stream.Collectors;

public class ProduitService {
    ModelMapper modelMapper = new ModelMapper();
    ProduitDAO produitDAO= new ProduitDAO();
    public void save(ProduitDto produitDto){
        produitDAO.save(modelMapper.map(produitDto, Produit.class));
    }
    public void  update(ProduitDto produitDto){
        produitDAO.update(modelMapper.map(produitDto, Produit.class));
    }
    public void  delete(ProduitDto produitDto){
        produitDAO.delete(modelMapper.map(produitDto, Produit.class));
    }
    public ProduitDto findById(int id){
        return (modelMapper.map(produitDAO.findById(id),ProduitDto.class));
    }

    public List<ProduitDto> findAll(){
        return produitDAO.findAll().stream().map(p -> modelMapper.map(p, ProduitDto.class))
                .collect(Collectors.toList());
    }
    public List<ProduitDto> findProducts() {
    	 return produitDAO.findProducts().stream().map(p -> modelMapper.map(p, ProduitDto.class))
                 .collect(Collectors.toList());
    }
}
