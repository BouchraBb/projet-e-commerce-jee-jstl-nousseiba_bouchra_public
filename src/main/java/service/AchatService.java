package service;

import dao.AchatDAO;
import dto.AchatDto;
import dto.ProduitDto;
import model.Achat;
import model.Produit;
import org.modelmapper.ModelMapper;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AchatService {
    AchatDAO achatDAO = new AchatDAO();
    ModelMapper modelMapper = new ModelMapper();
    public void  save(AchatDto achatDto){
        achatDAO.save(modelMapper.map(achatDto, Achat.class) );

    }

    public void update(AchatDto achatDto){
        achatDAO.update(modelMapper.map(achatDto, Achat.class) );
    }

    public void delete(AchatDto achatDto){
        achatDAO.delete(modelMapper.map(achatDto, Achat.class) );
    }

    public AchatDto findById(int id ){
        return modelMapper.map(achatDAO.findById(id), AchatDto.class);
    }

    public List<AchatDto> findAll(){
        return (ArrayList)achatDAO.findAll().stream().map(p -> modelMapper.map(p, AchatDto.class))
                .collect(Collectors.toList());
    }
    public AchatDto findAchatByProduit(int id, int idPanier) {
        Achat achat = achatDAO.findAchatByProduit(id, idPanier);
        if(achat!=null) {
            return modelMapper.map(achat, AchatDto.class);
        }
    	else return null;
    }
}
