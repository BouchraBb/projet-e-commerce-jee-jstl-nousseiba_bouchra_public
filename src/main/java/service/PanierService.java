package service;

import dao.PanierDAO;
import dto.ClientDto;
import dto.PanierDto;
import dto.ProduitDto;
import model.Client;
import model.Panier;
import model.Produit;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.stream.Collectors;

public class PanierService {
	ModelMapper modelMapper = new ModelMapper();

	PanierDAO panierDAO = new PanierDAO();

	public void save(PanierDto panierdto) {
		panierDAO.save(modelMapper.map(panierdto, Panier.class));
	}


	public void update(PanierDto panierdto) {
		panierDAO.update(modelMapper.map(panierdto, Panier.class));
	}

	public void delete(PanierDto panierdto) {
		panierDAO.delete(modelMapper.map(panierdto, Panier.class));
	}

	public PanierDto findById(int id ){
		return modelMapper.map(panierDAO.findById(id), PanierDto.class);
	}
	public List<PanierDto> findAll() {
		return panierDAO.findAll().stream().map(panier -> modelMapper.map(panier, PanierDto.class))
				.collect(Collectors.toList());
	}

	public PanierDto findByMailClient(String mail) {
		return modelMapper.map(panierDAO.findByMailClient(mail), PanierDto.class);
	}

	public double calculTotale(PanierDto panierSession) {
		double totale = 0.0;
		for (int i = 0; i < panierSession.getListeAchats().size(); i++) {
			totale += panierSession.getListeAchats().get(i).getQuantite()
					* panierSession.getListeAchats().get(i).getProduit().getPrix();
		}
		return totale;
	}

	public PanierDto getPanierSession(ClientDto clientSession) {

		PanierDto panierSession = findByMailClient(clientSession.getMail());
		if (panierSession != null) {
			return panierSession;
		} else {
			panierSession = new PanierDto(clientSession);
			save(panierSession);
			panierSession = findByMailClient(clientSession.getMail());
			return panierSession;
		}
	}
}
