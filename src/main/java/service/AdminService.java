package service;

import dao.AdminDAO;
import model.Admin;
import java.util.List;

public class AdminService {

	AdminDAO adminDAO = new AdminDAO();

	public void save(Admin admin) {
		adminDAO.save(admin);
	}

	public void update(Admin admin) {
		adminDAO.update(admin);
	}

	public void delete(Admin admin) {
		adminDAO.delete(admin);

	}

	public Admin findById(int id) {
		return adminDAO.findById(id);
	}

	public List<Admin> findAll() {
		return adminDAO.findAll();
	}

	public Admin findByMail(String mail, String mdp) {
		return adminDAO.findByMail(mail, mdp);
	}
}
