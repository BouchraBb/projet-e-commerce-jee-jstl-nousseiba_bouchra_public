package service;

import dao.ClientDAO;
import dto.ClientDto;
import model.Client;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.stream.Collectors;

public class ClientService {

    ModelMapper modelMapper = new ModelMapper();
    ClientDAO clientDAO= new ClientDAO();
    public void  save(ClientDto clientDto){

        clientDAO.save(modelMapper.map(clientDto,Client.class));
    }

    public void update(ClientDto clientDto){
        clientDAO.update(modelMapper.map(clientDto,Client.class));
    }

    public void delete(ClientDto clientDto){clientDAO.delete(modelMapper.map(clientDto,Client.class));

    }

    public List<ClientDto> findAll(){

         return clientDAO.findAll()
                 .stream().map(c -> modelMapper.map(c, ClientDto.class))
                 .collect(Collectors.toList());
    }
    public ClientDto findByMail(String mail, String mdp) {
        return (modelMapper.map(clientDAO.findByMail(mail, mdp),ClientDto.class)) ;
    }
    public ClientDto findByMailOnly(String mail) {
        return (modelMapper.map(clientDAO.findByMailOnly(mail),ClientDto.class)) ;
    }

    public void unsubscribe(ClientDto clientSession) {
        ClientDto clientDto = findByMailOnly(clientSession.getMail());
        clientDto.setMail(clientDto.getMail().substring(1, 3)+"**" +clientDto.getMail().substring(6, 8)+"*****");
        update(clientDto);
    }

    public ClientDto createClient(String nom, String prenom, String adresse, String mail, String tel, String motDePasse) {
        ClientDto clientDto= new ClientDto(nom, prenom, adresse, mail, tel, motDePasse);
        save(clientDto);
        return findByMail(mail, motDePasse);
    }
}
