package service;

import dao.CommandeDAO;
import dao.ProduitDAO;
import dto.ClientDto;
import dto.CommandeDto;
import dto.PanierDto;
import model.Client;
import model.Commande;
import model.Panier;
import model.Produit;
import org.modelmapper.ModelMapper;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;

public class CommandeService {

    CommandeDAO commandeDAO = new CommandeDAO();
    PanierService panierService = new PanierService();

    ModelMapper modelMapper = new ModelMapper();

    public void  save(CommandeDto commande){
        commandeDAO.save(modelMapper.map(commande, Commande.class));
    }
    public void update(Commande commande){
        commandeDAO.update(commande);
    }

    public void delete(Commande commande){
        commandeDAO.delete(commande);
    }
    public Commande findById(int id ){
        return commandeDAO.findById(id);
    }

    public List<Commande> findAll(){
        return commandeDAO.findAll();
    }

    public List<Commande> findAllCommandesByClient(String mail){
        return commandeDAO.findAllCommandesByClient(mail);
    }
    
    public CommandeDto validerPanier(PanierDto panierSession, double totale, ClientDto clientSession ) {
 
        Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());

        CommandeDto commande = new CommandeDto(totale , date, panierSession, clientSession);
        save(commande);
        panierSession.setValide(true);
        panierService.update(panierSession);
         return commande ;
    }


       
}
