package model;

import lombok.*;

import javax.persistence.*;

@Entity
@NamedQuery(name = "findAchatByProduit", query = "select a from Achat a where a.produit.idProduit = :idProduit and a.panier.idPanier= :idPanier")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Setter
@Getter
public class Achat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idAchat;
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name="idProduit")
    private Produit produit;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name="idPanier")
    private Panier panier;
    private Integer quantite;

    public Achat(Produit produit, int quantite, Panier panier) {
        this.produit = produit;
        this.quantite = quantite;
        this.panier = panier;
    }
    @Override
    public String toString() {
        return "Achat" +
                "idAchat=" + idAchat +
                ", produit=" + produit +
                ", quantite=" + quantite +
                '}';
    }

}
