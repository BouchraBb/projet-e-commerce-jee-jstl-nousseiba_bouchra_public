package model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@NamedQuery(name = "findAdminByMail", query = "select a from Admin a where a.mail = :mail and a.motDePasse = :mdp ")
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = { "mail" })})
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Setter
@Getter
public class Admin {
	
	 @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Integer idAdmin;
	    private String mail;
	    private String motDePasse ;

		public Admin(String mail, String motDePasse) {
			super();
			this.mail = mail;
			this.motDePasse = motDePasse;
		}

}
