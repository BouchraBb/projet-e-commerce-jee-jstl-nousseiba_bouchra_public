package model;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@NamedQueries({ @NamedQuery(name = "findByMailOnly", query = "select c from Client c where c.mail = :mail "),
@NamedQuery(name = "findClientByMail", query = "select c from Client c where c.mail = :mail and c.motDePasse = :mdp ")})
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = { "mail" })})
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Setter
@Getter
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idClient;
    private String nom;
    private String prenom;
    private String adresse;
    private String mail;
    private String motDePasse ;
    private String tel;

    @OneToMany(mappedBy = "client")
    private List<Panier> paniers;

    @OneToMany(cascade={CascadeType.PERSIST}, mappedBy = "client")
    private List<Commande> commandes = new ArrayList<>();

    public Client(String nom, String prenom, String adresse, String mail, String tel, String motDePasse) {
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.mail = mail;
        this.motDePasse= motDePasse;
        this.tel= tel ; 
    }
}
