package model;

import lombok.*;
import util.Constante;

import javax.persistence.*;
import java.sql.Date;

@Entity
@NamedQuery(name="findAllCommandesByClient",query = "select c from Commande c where c.client.mail = :mail")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Setter
@Getter
public class Commande {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idCommande;
    private Double total;
    private Date date;

    @OneToOne
    @JoinColumn(name = "idPanier")
    private Panier panier;

    @ManyToOne
    @JoinColumn(name="idClient", referencedColumnName="idClient")
    private Client client;

    public Commande(Double total, Date date, Panier panier,Client client) {
        this.total = total;
        this.date = date;
        this.panier = panier;
        this.client=client;
    }
}
