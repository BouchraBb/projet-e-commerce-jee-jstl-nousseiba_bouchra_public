package model;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@NamedQuery(
        name="findByMailClient",
        query="SELECT p FROM Panier p WHERE p.client.mail = :mail and p.valide is false")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Setter
@Getter
public class Panier {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idPanier;

    @OneToMany(mappedBy = "panier")
    private List<Achat> listeAchats= new ArrayList<>();

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idClient")
    Client client;
    @OneToOne(mappedBy = "panier")
    private Commande commande;
    private boolean valide ;

    public Panier(Client client) {
        this.client = client;
        this.valide=false;
    }

    public void addachat(Achat achat){
        listeAchats.add(achat);
    }

    public void removeachat(Achat achat){
        listeAchats.remove(achat);
    }

}
