package model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(
        name="findProducts",
        query="SELECT p FROM Produit p WHERE p.deleted is not true")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Setter
@Getter
public class Produit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idProduit;
    private String nom;
    private Double prix;
    private Integer quantiteDispo;
    private Boolean deleted;

    public Produit(String nom, Double prix, Integer quantiteDispo) {
        this.nom = nom;
        this.prix = prix;
        this.quantiteDispo = quantiteDispo;
        this.deleted=false;
      
    }
}
