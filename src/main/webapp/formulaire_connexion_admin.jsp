<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<link rel="stylesheet" href="style.css">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link
	href="https://fonts.googleapis.com/css2?family=Abril+Fatface&display=swap"
	rel="stylesheet">
<body>
<c:import url="menu_public.jsp"></c:import>
	<h2>
		<c:out value="connexion" />
	</h2>
	<div class="container">
		<form action="AdminServlet?operation=connect" method="post">

			<div class="row">
				<div class="col-25">
					<LABEL for="mail"><c:out value="Identifiant (email) * " />
					</LABEL>
				</div>
				<div class="col-75">
					<input type="text" name="mail" required>
				</div>
			</div>

			<div class="row">
				<div class="col-25">
					<LABEL for="motDePasse"><c:out value="Mot de passe * " />
					</LABEL>
				</div>
				<div class="col-75">
					<input type="text" name="motDePasse" required>
				</div>
			</div>

			<br>
			<div class="caption">
				<INPUT type="submit" value="Se connecter">
			</div>
		</form>
	</div>
	<br>
	<Hr>
	
	
	<br>
	<c:url value="/formulaire_inscription_admin.jsp"
		var="lienInscriptionAdmin" />
	<a href="${ lienInscriptionAdmin }"><c:out
			value="Créer un compte admin" /></a>


</body>
<c:import url="footer.jsp"></c:import>
</html>