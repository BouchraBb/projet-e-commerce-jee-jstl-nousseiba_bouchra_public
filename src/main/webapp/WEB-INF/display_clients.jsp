<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="style.css">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link
	href="https://fonts.googleapis.com/css2?family=Abril+Fatface&display=swap"
	rel="stylesheet">

<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<c:import url="menu_admin.jsp"></c:import>
	<h2>
		<c:out value="Clients" />
	</h2>


	<c:set scope="session" var="liste" value="${ clients }" />
	<div class="center-div">
		<table class="table table-striped " >
		<th><c:out value="Nom" /></th>
		<th><c:out value="Prénom" /></th>
		<th><c:out value="Adresse " /></th>
		<th><c:out value="Email" /></th>
		<th><c:out value="Téléphone" /></th>

		<c:forEach items="${ liste }" var="client">
			<tr>
				<td><c:out value="${ client.nom}" /></td>
				<td><c:out value="${ client.prenom}" /></td>
				<td><c:out value="${ client.adresse}" /></td>
				<td><c:out value="${ client.mail}" /></td>
				<td><c:out value="${ client.tel}" /></td>

			</tr>
		</c:forEach>

	</table>
</div>
</body>
</html>