<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="style.css">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link
	href="https://fonts.googleapis.com/css2?family=Abril+Fatface&display=swap"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<c:import url="menu_admin.jsp"></c:import>


	<h2>
		<c:out value=" Gérer les produits" />
	</h2>

	<br>
	<c:set scope="request" var="m" value="${message}" />
	<h6>
		<c:out value=" ${m}" />
	</h6>
	<c:set scope="session" var="liste" value="${ produits }" />
	<div class="center-div">
		<table class="table table-striped ">
		<th><c:out value="Nom" /></th>
		<th><c:out value="Prix" /></th>
		<th><c:out value="Stock" /></th>
		<th><c:out value="" /></th>
		<th><c:out value="" /></th>


		<c:forEach items="${ liste }" var="produit">
			<tr>
				<td><c:out value="${ produit.nom}" /></td>
				<td><c:out value="${ produit.prix}" /></td>
				<td><c:out value="${ produit.quantiteDispo}" /></td>

<c:choose>
						<c:when test="${ produit.deleted ==true }">
						<td><i class="bi bi-trash text-danger"></i></td>
						</c:when>
						
							<c:otherwise>
							<td><c:url value="/ProduitServlet" var="monLienSupprimer">
						<c:param name="operation" value="delete" />
						<c:param name="id" value="${ produit.idProduit}" />
					</c:url> <a class="a1" href="${ monLienSupprimer }">
					<i class="bi bi-trash"></i></a></td>
							
						</c:otherwise>
					</c:choose>
						
				<td><c:url value="/ProduitServlet?operation=update"
						var="monLienModifier">
						<c:param name="id" value="${ produit.idProduit}" />
					</c:url> <a class="a1" href="${ monLienModifier }">
					<i class="bi bi-arrow-clockwise"></i></a></td>

			</tr>
		</c:forEach>




	</table>
</div>
</body>
</html>