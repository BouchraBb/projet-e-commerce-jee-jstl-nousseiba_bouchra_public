<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<link rel="stylesheet" href="style.css">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link
	href="https://fonts.googleapis.com/css2?family=Abril+Fatface&display=swap"
	rel="stylesheet">
<body>
<c:import url="menu_public.jsp"></c:import>
	<h2>
		<c:out value="formulaire d'inscription du client" />
	</h2>
	<div class="container">
		<form action="ClientServlet?operation=create" method="post">

			<div class="row">
				<div class="col-25">
					<LABEL for="nom"><c:out value="Nom * " /> </LABEL>
				</div>
				<div class="col-75">
					<input type="text" name="nom" required>
				</div>
			</div>

			<div class="row">
				<div class="col-25">
					<LABEL for="prenom"><c:out value="Prénom " /> </LABEL>
				</div>
				<div class="col-75">
					<input type="text" name="prenom">
				</div>
			</div>

			<div class="row">
				<div class="col-25">
					<LABEL for="adresse"><c:out value="Adresse de livraison * " />
					</LABEL>
				</div>
				<div class="col-75">
					<input type="text" name="adresse" required>
				</div>
			</div>

			<div class="row">
				<div class="col-25">
					<LABEL for="tel"><c:out value="Numero de téléphone * " />
					</LABEL>
				</div>
				<div class="col-75">
					<input type="text" name="tel" required>
				</div>
			</div>

			<div class="row">
				<div class="col-25">
					<LABEL for="email"><c:out value="Adresse email" /> </LABEL>
				</div>
				<div class="col-75">
					<input type="text" name="email">
				</div>
			</div>
			<div class="row">
            				<div class="col-25">
            					<LABEL for="motDePass"><c:out value="Mot de passe * " /> </LABEL>
            				</div>
            				<div class="col-75">
            					<input type="text" name="motDePass" required>
            				</div>
            			</div>

            <div class="row">
                <div class="col-25">
                    <LABEL for="motDePassConf"><c:out value="Confirmer votre mot de passe * " /> </LABEL>
                </div>
                <div class="col-75">
                    <input type="text" name="motDePassConf" required>
                </div>
            </div>

			<br>
			<div class="caption">
				<INPUT type="submit" value="Envoyer ma demande">
			</div>
		</form>
	</div>
	<br>
	<Hr>

	<%-- <c:url value="/ClientServlet" var="monLien"> --%>
	<%-- <c:param name="operation" value="afficher"/> --%>
	<%-- </c:url> --%>
	<%-- <a href="${ monLien }"><c:out value="liste des personnes" /></a> --%>

</body>
</html>