<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ page isELIgnored="false"%>

<!DOCTYPE html>
<html>
<head>

<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link
	href="https://fonts.googleapis.com/css2?family=Abril+Fatface&display=swap"
	rel="stylesheet">

<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
<link rel="stylesheet" href="style.css">
<meta charset="ISO-8859-1">
<title>monPanier</title>
</head>
<body>
	<c:import url="menu_client.jsp"></c:import>
	<c:set var="panier" value="${ panierSession }" />
	<c:set var="liste" value="${ panier.listeAchats}" />
	<c:set var="totale" value="${totale}" />
	<h2>
		<c:out value="Mon panier" />
	</h2>


	<div class="center-div">
		<table class="table table-striped ">
			<th><c:out value="Article" /></th>
			<th><c:out value="Prix" /></th>
			<th><c:out value="Retirer" /></th>
			<th><c:out value="Quantite " /></th>
			<th><c:out value="Ajouter" /></th>



			<c:forEach items="${ liste }" var="achat">

				<tr>
					<td><c:out value="${ achat.produit.nom}" /></td>
					<td><c:out value="${ achat.produit.prix}" /></td>
					<td><c:url value="/PanierServlet" var="monLienDiminuer">
							<c:param name="operation" value="reduceQuantity" />
							<c:param name="idAchat" value="${ achat.idAchat}" />
						</c:url> <a class="a1" href="${ monLienDiminuer }"><i
							class="bi bi-dash-circle"></i></a></td>

					<td><c:out value="${ achat.quantite }" /></td>

					<c:choose>
						<c:when test="${ achat.produit.quantiteDispo>0 }">

							<td><c:url value="/PanierServlet" var="monLienAjouter">
									<c:param name="operation" value="addQuantity" />
									<c:param name="idAchat" value="${ achat.idAchat}" />
								</c:url> <a class="a1" href="${ monLienAjouter }"><i
									class="bi bi-plus-circle"></i></a></td>
						</c:when>

						<c:otherwise>
							<td><i class="bi bi-plus-circle text-danger"></i></td>
						</c:otherwise>
					</c:choose>

				</tr>
			</c:forEach>
				<tr>
					<td style="font-weight:bold"><c:out value="Totale"/></td>
					<td><c:out value="" /></td>
					<td><c:out value="" /></td>
					<td><c:out value="" /></td>
					<td style="font: italic 100 20px/1 'Abril Fatface'"><c:out value="${ totale }"/><c:out value=" €" /></td>
				</tr>
		</table>
	</div>
	<div class="center-div">
		<br> <br>
		<div class="center-div">
			<button class="btn btn-outline-success my-2 my-sm-0" type="submit">

				<c:url value="/CommandeServlet" var="lienCommander">
					<c:param name="operation" value="payer"></c:param>
				</c:url>
				<a class="a1" href="${ lienCommander }"><c:out
						value="Passer au paiement " /> </a>
			</button>
		</div>
	</div>
</body>
</html>