<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<link rel="stylesheet" href="style.css">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link
	href="https://fonts.googleapis.com/css2?family=Abril+Fatface&display=swap"
	rel="stylesheet">
<body>
	<c:import url="menu_client.jsp"></c:import>
	<c:set var="totale" value="${totale}" />
	<c:set var="panier" value="${ panierSession }" />
	<c:set var="client" value="${ clientSession }" />
	<h2>
		<c:out value="formulaire du paiement" />
	</h2>

	<div class="container">
		<form action="CommandeServlet?operation=commander" method="post">
			<div class="row">
				<div class="col-25">
					<LABEL for="total"><c:out value="Total de la commande:  " />
					</LABEL>
				</div>
				<div class="col-75">
					<label type="text"><c:out value="${totale }" />€ </LABEL>
				</div>
			</div>

			<h5>
				<c:out value="Verification de vos informations" />
			</h5>
			<div class="row">
				<div class="col-25">
					<LABEL for="adresse"><c:out value="Adresse de livraison * " />
					</LABEL>
				</div>
				<div class="col-75">
					<input type="text" name="adresse" value="${client.adresse }"
						required>
				</div>
			</div>

			<div class="row">
				<div class="col-25">
					<LABEL for="tel"><c:out value="Numero de téléphone * " />
					</LABEL>
				</div>
				<div class="col-75">
					<input type="text" name="tel" value="${client.tel }" required>
				</div>
			</div>

			<div class="row">
				<div class="col-25">
					<LABEL for="email"><c:out value="Adresse email" /> </LABEL>
				</div>
				<div class="col-75">
					<input type="text" name="email" value="${client.mail }">
				</div>
			</div>
			<h5>
				<c:out value="Informations de paiement" />
			</h5>
			<div class="row">
				<div class="col-25">
					<LABEL for="cb"><c:out value="Numero de carte bancaire" />
					</LABEL>
				</div>
				<div class="col-75">
					<input type="text" name="cb">
				</div>
			</div>

			<div class="row">
				<div class="col-25">
					<LABEL for="dateExpir"><c:out value="Date d'expiration" />
					</LABEL>
				</div>
				<div class="col-75">
					<input type="date" name="dateExpir">
				</div>
			</div>

			<div class="row">
				<div class="col-25">
					<LABEL for="code"><c:out value="Code crypto" /> </LABEL>
				</div>
				<div class="col-75">
					<input type="number" name="code">
				</div>
			</div>
			<br>
			<div class="caption">
				<INPUT type="submit" value="Envoyer ma demande">
			</div>
		</form>
	</div>
	<br>
	<Hr>

	<%-- <c:url value="/ClientServlet" var="monLien"> --%>
	<%-- <c:param name="operation" value="afficher"/> --%>
	<%-- </c:url> --%>
	<%-- <a href="${ monLien }"><c:out value="liste des personnes" /></a> --%>

</body>
</html>