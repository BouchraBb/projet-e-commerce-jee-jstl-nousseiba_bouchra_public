<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="style.css">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link
	href="https://fonts.googleapis.com/css2?family=Abril+Fatface&display=swap"
	rel="stylesheet">

<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">

<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<c:import url="menu_client.jsp"></c:import>

<div id="main">

	<br>

	<c:set scope="session" var="liste" value="${ produits }" />
	<div class="center-div">
	<table class="table table-striped">
		<th><c:out value="Nom" /></th>
		<th><c:out value="Prix" /></th>
		<th><c:out value="Quantite Disponible" /></th>
		<th><c:out value="Ajouter dans mon panier" /></th>



		<c:forEach items="${ liste }" var="produit">
			<tr>
				<td><c:out value="${ produit.nom}" /></td>
				<td><c:out value="${ produit.prix}" /></td>
				<td><c:out value="${ produit.quantiteDispo}" /></td>

				<c:choose>
					<c:when test="${ produit.quantiteDispo>0 }">

						<td><c:url value="/PanierServlet" var="monLienAjouter">
								<c:param name="operation" value="addProduct" />
								<c:param name="id" value="${ produit.idProduit}" />
							</c:url> <a class="a1" href="${ monLienAjouter }"><i
								class="bi bi-plus-circle"></i> </a></td>
					</c:when>

					<c:otherwise>
						<td><i class="bi bi-plus-circle text-danger"></i></td>
					</c:otherwise>
				</c:choose>
			</tr>
		</c:forEach>

	</table>
	</div>
</div>
</body>
</html>