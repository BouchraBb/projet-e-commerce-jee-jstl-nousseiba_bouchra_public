<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@page isELIgnored="false"%>

<link rel="stylesheet" href="./pstyle.css">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link
	href="https://fonts.googleapis.com/css2?family=Abril+Fatface&display=swap"
	rel="stylesheet">
<body height="auto" width="750xp">
	<c:import url="menu_client.jsp"></c:import>
	<c:set scope="session" var="com" value="${ commande }" />
	<c:set scope="session" var="c" value="${ clientSession }" />
	<c:set scope="request" var="panier" value="${ panierValide }" />
	<c:set scope="session" var="totale" value="${totale}" />

	<p>
		<c:out value="Client : " />
		&nbsp;
		<c:out value=" ${ c.nom }" />
		<br>
		<c:out value="Commande numero : " />
		&nbsp;
		<c:out value="${ com.idCommande} " />
		<br>
		<c:out value="Date de commande : " />
		&nbsp;
		<c:out value="${ com.date} " />
		<br>
	<hr>
	<c:out value="Articles  " />
	<Hr>
	<c:set var="liste" value="${panier.listeAchats}" />

	<table class="table1">
		<th><c:out value="Article" /></th>
		<th><c:out value="Prix" /></th>
		<th><c:out value="Quantite " /></th>
		<th><c:out value="Remise" /></th>

		<c:forEach items="${ liste }" var="achat">
			<tr>
				<td><c:out value="${ achat.produit.nom}" /></td>
				<td><c:out value="${ achat.produit.prix}" /></td>
				<td><CENTER>
						<c:out value="${ achat.quantite}" />
					</CENTER></td>
				<td><c:out value="" /></td>
			</tr>
		</c:forEach>
		<tr>
			<c:out value="TOTAL:" />
			<c:out value="${ totale }" />
			<c:out value=" €" />
		</tr>


	</table>

	<br>
</body>
</html>
