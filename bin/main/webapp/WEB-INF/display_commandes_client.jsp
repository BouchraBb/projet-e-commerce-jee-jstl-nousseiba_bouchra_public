<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="style.css">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link
	href="https://fonts.googleapis.com/css2?family=Abril+Fatface&display=swap"
	rel="stylesheet">

<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<c:import url="menu_client.jsp"></c:import>
	
	<c:set scope="session" var="liste" value="${ commandes }" />
	<div class="center-div">
		<table class="table table-striped table-pading">
		<th><c:out value="Numéro de commande" /></th>
		<th><c:out value="Totale" /></th>
		<th><c:out value="Date" /></th>

		<c:forEach items="${ liste }" var="commande">
			<tr>
				<td><c:out value="${ commande.idCommande}" /></td>
				<td><c:out value="${ commande.total}" /></td>
				<td><c:out value="${ commande.date}" /></td>

			</tr>
		</c:forEach>
	</table>
	</div>

</body>
</html>