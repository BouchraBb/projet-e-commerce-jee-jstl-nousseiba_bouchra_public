<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>

<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link
	href="https://fonts.googleapis.com/css2?family=Abril+Fatface&display=swap"
	rel="stylesheet">
	<link rel="stylesheet" href="style.css">
<body >
<c:import url="menu_public.jsp"></c:import>
<div id="main">
<c:set scope="request" var="m" value="${message }" />

<h6 class=error style="color:red"> <c:out value="${m } " /> </h6>
	<h6>
		<c:out value="Vous avez déja un compte  " />
		
	</h6>
	<div class="container">
		<form action="ClientServlet?operation=connect" method="post">

			<div class="row">
				<div class="col-25">
					<LABEL for="email"><c:out value="Identifiant (email) * " />
					</LABEL>
				</div>
				<div class="col-75">
					<input type="text" name="email" required>
				</div>
			</div>

			<div class="row">
				<div class="col-25">
					<LABEL for="motDePasse"><c:out value="Mot de passe * " />
					</LABEL>
				</div>
				<div class="col-75">
					<input type="text" name="motDePasse" required>
				</div>
			</div>

			<br>
			<div class="caption">
				<INPUT type="submit" value="Se connecter">
			</div>
		</form>
	</div>
	<br>
	<Hr>
		<h6>
		<c:out value="Vous n'avez pas encore de compte  " />
	</h6>
		<c:url value="/formulaire_inscription.jsp" var="lienInscriptionClient" />
	<a href="${ lienInscriptionClient }"><c:out
			value="Créer un compte client" /></a>
</div>
</body>
<c:import url="footer.jsp"></c:import>
</html>